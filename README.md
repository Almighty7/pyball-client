PyBall util files
===
This repository contains files that are needed for the PyBall eclipse plugin
to have full functionality. 
[Other Repositories](https://gitlab.com/groups/Almighty7)

Installation packages can be found here:
[rpm](https://lukor.tk/files/wallapy/Walla.py-1.1.2-1.x86_64.rpm)
[deb](https://lukor.tk/files/wallapy/walla.py_1.1.2-1_amd64.deb)
Mac: brew install LlinksRechts/-/wallapy
