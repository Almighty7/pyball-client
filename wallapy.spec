Name:	Walla.py	
Version:	1.1.1
Release:	1
Summary:	The library that supports the PyBall Eclipse plugin

Group:		Development/Tools
License:	No
URL:		http://wallapy.lukor.tk
Source0:	walla.py.tar.gz

Requires:	nmap

%define debug_package %{nil}
%define _unpackaged_files_terminate_build 0

%description
Walla.py is the library that supplies utility functions for the Walla.py Eclipse plugin. It is used to prepare a Wallaby for use with the plugin, as well as retrieve any connected Wallabys.

%prep
%setup


%build


%install
mkdir -p $RPM_BUILD_ROOT/usr/share/pyball
mkdir -p $RPM_BUILD_ROOT/usr/bin
install -m 755 setupWallaby $RPM_BUILD_ROOT/usr/bin/setupWallaby
install -m 755 getDevices $RPM_BUILD_ROOT/usr/bin/getDevices
install -m 755 nwAddress $RPM_BUILD_ROOT/usr/bin/nwAddress
cd wallaby
find * -type d | while read file; do
	mkdir $RPM_BUILD_ROOT/usr/share/pyball/$file
done
find * -type f | while read file; do
	install -m 644 $file $RPM_BUILD_ROOT/usr/share/pyball/$file
done

%files
%attr(755, root, root) /usr/bin/setupWallaby
%attr(755, root, root) /usr/bin/getDevices
%attr(755, root, root) /usr/bin/nwAddress
%dir %attr(755, root, root) /usr/share/pyball/
%attr(755, root, root) /usr/share/pyball/py_compile.py
%dir %attr(755, root, root) /usr/share/pyball/usrbin
%attr(755, root, root) /usr/share/pyball/usrbin/getname
%attr(755, root, root) /usr/share/pyball/usrbin/compile
%attr(755, root, root) /usr/share/pyball/usrbin/run
%attr(755, root, root) /usr/share/pyball/usrbin/clearProject
%dir %attr(755, root, root) /usr/share/pyball/wallapy
%attr(644, root, root) /usr/share/pyball/wallapy/sensors.py
%attr(644, root, root) /usr/share/pyball/wallapy/util.py
%attr(644, root, root) /usr/share/pyball/wallapy/servos.py
%attr(644, root, root) /usr/share/pyball/wallapy/motors.py
%attr(644, root, root) /usr/share/pyball/wallapy/walla.py



%changelog
*Fri Mar 04 2016 - Lukas Rysavy - lukas@rysavy.net

- Changed setup script to accept ssh keys
  instead of using ones that are in the package
