'''
initial c library by Alexander Halbart
all methods ported to python by Manuel Reinsperger
modifications by Lukas Rysavy
'''
from util import register_create
import serial
import time


def create_connect():
    """
    @author: Lukas Rysavy
    @summary: creates a new Create object
    @return: the Create object
    @rtype: Create
    """
    return Create("/dev/ttyUSB0")


class Create:
    SENSOR_BUMPWHEEL = 7
    SENSOR_WALL = 8
    SENSOR_CLIFF_LEFT = 9
    SENSOR_CLIFF_LEFT_FRONT = 10
    SENSOR_CLIFF_RIGHT_FRONT = 11
    SENSOR_CLIFF_RIGHT = 12
    SENSOR_VIRTUAL_WALL = 13
    SENSOR_OVERCURRENTS = 14
    SENSOR_IR_BYTE = 17
    SENSOR_BUTTONS = 18
    SENSOR_DISTANCE = 19
    SENSOR_ANGLE = 20
    SENSOR_CHARGING_STATE = 21
    SENSOR_VOLTAGE = 22
    SENSOR_CURRENT = 23
    SENSOR_BATTERY_TEMPERATURE = 24
    SENSOR_BATTERY_CHARGE = 25
    SENSOR_BATTERY_CAPACITY = 26
    SENSOR_WALL_SIGNAL = 27
    SENSOR_CLIFF_LEFT_SIGNAL = 28
    SENSOR_CLIFF_LEFT_FRONT_SIGNAL = 29
    SENSOR_CLIFF_RIGHT_FRONT_SIGNAL = 30
    SENSOR_CLIFF_RIGHT_SIGNAL = 31
    SENSOR_USER_DIGITAL_INPUTS = 32
    SENSOR_USER_ANALOG_INPUTS = 33
    SENSOR_CHARGING_SOURCES_AVAILABLE = 34
    SENSOR_OI_MODE = 35
    SENSOR_SONG_NUMBER = 36
    SENSOR_SONG_PLAYING = 37
    SENSOR_NUMBER_OF_STREAM_PACKETS = 38
    SENSOR_VELOCITY = 39
    SENSOR_RADIUS = 40
    SENSOR_VELOCITY_RIGHT = 41
    SENSOR_VELOCITY_LEFT = 42

    connection = None

    def __init__(self, port):
        """
        @author: Manuel Reinsperger
        @summary: connect to Create
        @Sends: 128 Start
                132 Enable full mode
        """
        try:
            self.connection = serial.Serial(port, baudrate=115200, timeout=1)
            self.send_command_ASCII('128')
            self.send_Command_ASCII('132')
            register_create(self)
        except:
            self.connection = None

    def send_command_ASCII(self, command):
        """
        @author: Manuel Reinsperger
        @summary: take an ASCII string, split it by whitespace and send it via sendCommandRaw
        @args: command (string): command to send
        """
        cmd = ""
        for v in command.split():
            cmd += chr(int(v))

        self.send_command_raw(cmd)

    def send_command_raw(self, command):
        """
        @author: Manuel Reinsperger
        @summary: take string interpreted as a byte array and sends it to create
        @args: command (char): command to send
        @return: 1 success, -1 not connected, -2 connection lost
        @rtype: int
        """
        try:
            if self.connection is not None:
                self.connection.write(command)
                return 1
            else:
                return -1
        except serial.SerialException:
            self.connection = None
            return -2

    def stop_all(self):
        """
        @author: Manuel Reinsperger
        @summary: stop the create
        @aends: 145 0 0 0 0 stop both motors
        """
        self.send_command_ASCII('145 0 0 0 0')

    def drive_distance(self, speed, dist):
        """
        @author: Manuel Reinsperger
        @summary: drive the distance with speed, must be both positive or negative
        @args: speed (int): average speed in mm/s
               dist (int): distance in mm
        @sends: 152         Define of external script
                13          Number of bytes
                145         Drive Direct command
                speed >> 8  Speed right high byte (bits 8-15)
                speed       Speed right low byte (bits 0-7)
                speed >> 8  Speed left high byte (bits 8-15)
                speed       Speed left low byte (bits 0-7)
                156         Wait for distance (in mm)
                dist >> 8   Distance high byte (bits 8-15)
                dist        Distance low byte (bits 0-7)
                145 0 0 0 0 Stop both motors
                153         Start external script
        """
        if speed * dist < 0:
            dist = dist * -1
        speedlow = str(speed & 0xFF)
        speedhigh = str((speed & 0xFF00) >> 8)
        distlow = str(dist & 0xFF)
        disthigh = str((dist & 0xFF00) >> 8)
        print ('152 13 145 %s %s %s %s 156 %s %s 145 0 0 0 0 153' %
               (speedhigh, speedlow, speedhigh, speedlow, disthigh, distlow))
        self.send_command_ASCII('152 13 145 %s %s %s %s 156 %s %s 145 0 0 0 0 153' %
                                (speedhigh, speedlow, speedhigh, speedlow, disthigh, distlow))

    def drive_distance_duration(self, speed, dist):
        """
        @author: Manuel Reinsperger
        @summary: calculate the approximate time that driving will take
        @args: speed (int): average speed in mm/s
               dist (int): distance in mm
        @return: duration in seconds
        @rtype: int
        """
        return (int)(dist / speed)

    def drive_distance_wait(self, speed, dist):
        """
        @author: Manuel Reinsperger
        @summary: use the driveDistance and return after finished driving
        @args: speed (int): average speed in mm/s
               dist (int): distance in mm
        """
        self.drive_distance(speed, dist)
        time.sleep(self.drive_distance_duration(speed, dist))

    def spin_angle(self, speed, angle):
        """
        @author: Manuel Reinsperger
        @summary: turn in place for a certain angle
        @args: speed (int): average speed in mm/s
               dist (int): distance in mm
        @sends: 152             Define of external script
                13              Number of bytes
                137             Drive command
                speed >> 8      Speed high byte (bits 8-15)
                speed           Speed low byte (bits 0-7)
                direction >> 8  Direction high byte (bits 8-15)
                direction       Direction low byte (bits 0-7)
                157             Wait for angle (in degrees)
                angle >> 8      Angle high byte (bits 8-15)
                angle           Angle low byte (bits 0-7)
                145 0 0 0 0     Stop both motors
                153             Start external script
        """
        angle = angle * -1
        if angle > 0:  # 0x0001
            dirlow = str(0x01)
            dirhigh = str(0x00)
        else:  # 0xFFFF
            dirlow = str(0xFF)
            dirhigh = str(0xFF)
        speedlow = str(speed & 0xFF)
        speedhigh = str((speed & 0xFF00) >> 8)
        anglelow = str(angle & 0xFF)
        anglehigh = str((angle & 0xFF00) >> 8)

        self.send_command_ASCII('152 13 137 %s %s %s %s 157 %s %s 145 0 0 0 0 153' %
                                (speedhigh, speedlow, dirhigh, dirlow, anglehigh, anglelow))

    def spin_angle_duration(self, speed, angle):
        """
        @author: Manuel Reinsperger
        @summary: calculate the approximate time that turning will take
        @args: speed (int): average speed in mm/s
               dist (int): distance in mm
        @return: duration in seconds
        @rtype: int
        """
        return (int)(2.2689280275 * abs(angle) / speed)  # (d*pi*angle/360/speed)

    def spin_angle_wait(self, speed, angle):
        """
        @author: Manuel Reinsperger
        @summary: use the spinAngle and return after finished turning
        @args: speed (int): average speed in mm/s
               dist (int): distance in mm
        """
        self.spin_angle(speed, angle)
        time.sleep(self.spin_angle_duration(speed, angle))

    def poll_sensor(self, sensor):
        """
        @author: Manuel Reinsperger
        @summary: poll create for sensor value
        @args: sensor (int): number of sensor, see documentation for more
        @return: sensor value
        @rtype: int
        """
        sizesList = '000000111111111111221221222222212111112222'

        self.send_command_ASCII('142 ' + str(sensor))
        if 6 < sensor < 42:         # Work on basic sensors, not groups
            valu = 0
            for i in range(ord(sizesList[sensor]) - ord('0')):
                while self.connection.inWaiting() is 0:
                    time.sleep(0.025)

                valu = (valu << 8) | ord(self.connection.read(1))

            return valu
        else:
            return -1

    def set_mode(self, mode):
        """
        @author: Manuel Reinsperger
        @summary: sets mode to safe or full
        @args: mode (int): mode type, 0 for safe, 1 for full
        """
        self.send_command_ASCII(chr(131 + mode))

    def drive_direct(self, rightmotor, leftmotor):
        """
        @author: Manuel Reinsperger
        @summary: start driving with motor speeds
        @args: speed (int): speed in mm/s
        @sends: 145   Drive Direct command
                rightmotor >> 8 Speed right high byte (bits 8-15)
                rightmotor      Speed right low byte (bits 0-7)
                leftmotor >> 8  Speed left high byte (bits 8-15)
                leftmotor       Speed left low byte (bits 0-7)
        """
        rightspeedlow = str(rightmotor & 0xFF)
        rightspeedhigh = str((rightmotor & 0xFF00) >> 8)
        leftspeedlow = str(leftmotor & 0xFF)
        leftspeedhigh = str((leftmotor & 0xFF00) >> 8)
        self.send_command_ASCII('145 %s %s %s %s' %
                                (rightspeedlow, rightspeedhigh, leftspeedlow, leftspeedhigh))

    def drive_circle(self, speed, radius):
        """
        @author: Manuel Reinsperger
        @summary: start driving with motor speeds
        @args: speed (int) in mm/s
        @sends: 137   Drive Direct command
                speed >> 8  Speed right high byte (bits 8-15)
                speed       Speed right low byte (bits 0-7)
                radius >> 8 Speed left high byte (bits 8-15)
                radius      Speed left low byte (bits 0-7)
        """
        speedlow = str(speed & 0xFF)
        speedhigh = str((speed & 0xFF00) >> 8)
        radiuslow = str(radius & 0xFF)
        radiushigh = str((radius & 0xFF00) >> 8)
        self.send_command_ASCII('137 %s %s %s %s' %
                                (speedlow, speedhigh, radiuslow, radiushigh))
